(**Modules containing the Ai*)

(** Exceptions used in the code*)

exception Pascettedirection (*ce manchot ne peut pas aller par là*)
exception Peutpasbouger     (* ce manchot ne peut pas bouger*)
exception FatalError of string (* autre erreur, non rattrapée par le code*)

(** Functions*)

(**Function to determine the number of fish on a case *)
val nb_poissons :  Move.elt Move.grid -> Move.pos -> int


(**  Return the element of maximum value and its index
 * in a list of couple (position, value) *)
val max_list_pos : (Move.pos * int) list -> (Move.pos * int * int)

(**Transforme an array of position into an array of couple 
  * (position, value) where the value is computed with the given function*)
val position_value : Move.pos list -> (Move.pos -> int) -> (Move.pos * int) list

(**Function which compute the reachable position in a given direction*)
val accessible : Move.elt Move.grid -> Move.pos -> Move.dir -> Move.pos list

(**Functions which return the best possible move in a given direction from a given position
  * where the value of a move is computed by a given function*)
val best_of_dir : Move.elt Move.grid -> Move.pos ->
  (Move.pos -> int) -> Move.dir -> (Move.move * int)

(** Return the element of maximum value
 * in an array of couple (move, value) *)
val max_list_dir : (Move.move * int) list -> (Move.move * int)

(**Function  which return the best move to do from a given position *)
val penguin_best_choice : Move.elt Move.grid ->
  Move.pos -> (Move.pos -> int) -> (Move.move * int)


(** Choose the penguin with the best possible move in a list.
  * The value of a move is computed by  a given function*)
val best_penguin : Move.elt Move.grid ->
  Move.pos list -> (Move.pos -> int) -> (Move.pos * Move.move)

(**Current ai function*)
val ai :Move.elt Move.grid -> Move.pos list -> (Move.pos * Move.move)

(**alternative ai function, which only move one case at a time*)

val ai2: Move.elt Move.grid -> Move.pos list -> (Move.pos * Move.move)


(**Other AI which choose randomly ammong its possibility*)
val ai3: Move.elt Move.grid -> Move.pos list -> (Move.pos * Move.move)

